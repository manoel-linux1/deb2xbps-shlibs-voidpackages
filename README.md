# DEB2XBPS is no longer being maintained                                                                                                                                              

- deb2xbps-version: oct 2023

- build-latest: 0.5.2

- Support for the distro: Void-Linux

- It has been unlocked, now you can use deb2xbps without sudo or superuser.

- The support for 32-bit .deb packages has been discontinued
- If you try to install the 32-bit .deb package, it may be converted, but it will not be installed due to the discontinued support for 32-bit.
- The support for 32-bit in deb2xbps has also been discontinued

- It is recommended to create a separate directory for installing .deb packages using DEB2XBPS. If you do not create a dedicated directory, deb2xbps may create files and folders in your current working directory, which can make it difficult to manage and remove them later on

- When using DEB2XBPS, it may create a "shlibs" directory that contains the necessary libraries (shlibs) required for installing the .deb packages. If you accidentally create this directory, you can delete it without causing any harm to your system or the functionality of DEB2XBPS

- DEB2XBPS is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with DEB2XBPS, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of DEB2XBPS to add additional features

# Based on XDEB

- DEB2XBPS is a project based on the code of xdeb, a powerful package conversion tool. xdeb was a fundamental source of inspiration for the development of DEB2XBPS, and I would like to express my gratitude to the xdeb team for their incredible work. The original source code of xdeb can be found at: https://github.com/toluschr/xdeb.

## Installation

- To install DEB2XBPS, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/deb2xbps-shlibs-voidpackages.git

# 2. To install the DEB2XBPS script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# 3. Execute the DEB2XBPS script

- mkdir `example`

- Download the .deb package you want to install and place it inside the directory 

- sudo deb2xbps -Syu `example.deb` 

- Use the following command to install: `sudo xbps-install -R binpkgs exemple`

- For remove: `sudo xbps-remove -R exemple`

- This will launch the DEB2XBPS tool, allowing you to convert .deb packages to .xbps format

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `/uninstall.sh`

# Help 
# -y                          Automatic dependencies
# -S                          Sync dependency file
# -q                          Don't build the package at all
# -C                          Clean all files
# -b                          No extract, just build files in destdir
# -u                          Remove empty directories
# -i                          Ignore file conflicts
# -f                          Attempt to automatically fix common conflicts
# --deps=...                  Add manual dependencies
# --arch=...                  Add an architecture for the package to run on
# --rev=...                   Set package revision. Alternative to -y
# --help                      Show this help page

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The DEB2XBPS project is currently in development. The latest stable version is 0.5.2. We aim to provide regular updates and add more features in the future.

# License

- DEB2XBPS is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of DEB2XBPS.
